class ArithmeticProgression:
    def __init__(self, a1, b):
        self.razn = b
        self.first_num = a1

    def construct(self, n):
        l = [self.first_num]
        for i in range(1, n):
            l.append(l[i-1]+self.razn)
        return l

    def sum_of_progression(self, n):
        return sum(self.construct(n))

    def n_numb_in_prog(self, n):
        l = self.construct(n)
        return l[n-1]
        
def linear_equation(k, b):
    if k != 0 and b != 0:
        return -b/k
    elif k != 0 and b == 0:
        return 0
    return None
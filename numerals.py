def toHex(n):
    return hex(n)[2:]

def toOct(n):
    return oct(n)[2:]

def toBin(n):
    return bin(n)[2:]